import React, { useEffect, useState } from "react"
import './App.css'
import Rectangle from './Rectangle'

const App = () => {

    const [isPressed, setIsPressed] = useState(false)
    
    const pressHandler = () => {
        setIsPressed(true)
    }

    return (
        <div className='container'>
            <div className='hello'>
                <p>Hello</p>
                <p>Hello Hello</p>
                <p>Hello Hello Hello</p>
                <p>Hello Hello Hello Hello</p>
            </div>
            <div className='rectangleContainer'>
                <Rectangle pressed={isPressed}/>
            </div>

                <button className="button" onClick = {pressHandler}>Press</button><br/>

                {isPressed ? <p className='text'>Button Clicked</p> : null}

        </div>
    )
}

export default App
