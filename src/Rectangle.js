import React, { useEffect, useState } from "react";
import './App.css'

const Rectangle = ({pressed}) => {
    return (
        <div >
            <div className={pressed ? 'greenRectangle' : 'redRectangle'}>
            </div>
        </div>
    )
}

export default Rectangle
